package ru.scheredin.services;

import ru.scheredin.dto.Employee;

import java.util.List;

public interface EmployeesService {
    Employee getRandomEmployee();
}
